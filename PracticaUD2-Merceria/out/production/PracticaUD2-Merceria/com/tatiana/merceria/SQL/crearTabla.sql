DELIMITER //
CREATE PROCEDURE crearTablaMerceria1()
BEGIN
    create table ventaUno(
                             id_venta int primary key auto_increment ,
                             nombre_vendedor varchar(50),
                             referencia varchar(50),
                             cantidad varchar(50),
                             id_producto int not null,
                             id_proveedor int not null,
                             precio float,
                             fecha_factura date
    );
--
    create table proveedoresUno(
                                   id_proveedor int primary key auto_increment,
                                   nombre_proveedor varchar (50),
                                   telefono varchar(50),
                                   origen_producto varchar(100),
                                   tipo_producto varchar(50),
                                   empresa_proveedor varchar(50)
    );
--

    create table productosUno(
                                 id_producto int primary key auto_increment,
                                 nombre_producto varchar(50),
                                 color_producto varchar(50),
                                 marcaProducto varchar(50),
                                 tallaProducto varchar(50),
                                 fabricante varchar(50)
    );


    alter table ventaUno
        add foreign key(id_producto)references productos(id_producto),
        add foreign key(id_proveedor)references proveedores(id_proveedor);
END //