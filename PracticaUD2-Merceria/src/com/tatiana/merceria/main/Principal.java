package com.tatiana.merceria.main;

import com.tatiana.merceria.gui.Controlador;
import com.tatiana.merceria.gui.Modelo;
import com.tatiana.merceria.gui.Vista;

import java.sql.SQLException;

public class Principal {
    public static void main(String[] args) throws SQLException {
        Modelo modelo= new Modelo();
        Vista vista= new Vista();
        Controlador controlador= new Controlador(modelo,vista);
    }

}
