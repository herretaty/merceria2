package com.tatiana.merceria.base.enums.productos;

public enum MarcaProductos {
    Admas("ADMAS"),
    AntonioMiró("ANTONIO MIRÔ"),
    Azzaro("AZZARO"),
    Blackspade("BLACKSPADE"),
    BLANCAHERNANDEZ("BLANCA HERNANDEZ"),
    CalvinKlein("CALVIN KLEIN"),
    Dior("DIOR"),
    versace("VERSACE"),
    CrazyFarm("CRAZYFARM"),
    Disney("DISNEY"),
    JandJBrothers("J&J BROTHERS"),
    Lois("LOIS");

    private String valor;

    MarcaProductos(String valor){
        this.valor= valor;
    }

    public String getValor() {
        return valor;
    }
}
