package com.tatiana.merceria.base.enums.productos;

public enum FabricanteProductos {
madeinchina("MADE IN CHINA"),
    alibaba("ALIBABA"),
    globalsource("GLOBAL SOURCE"),
    indiamark("INDIAMART"),
    ngapparels("NG APPARELS"),
    loyaltextiles("LOYAL TEXTILES"),
    suuchi("SUUCHI"),
    mandana("MANDANA");

    private String valor;

    FabricanteProductos(String valor){
        this.valor= valor;

    }

    public String getValor() {
        return valor;
    }
}
