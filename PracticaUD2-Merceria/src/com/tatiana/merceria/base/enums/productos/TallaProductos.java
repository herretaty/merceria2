package com.tatiana.merceria.base.enums.productos;

public enum TallaProductos {
    xxs("XXS"),
    xs("XS"),
    s("S"),
    m("M"),
    l("L"),
    xl("XL"),
    xxl("XXL"),
    xxxl("XXXL");

    private String valor;

    TallaProductos(String valor){
        this.valor= valor;
    }

    public String getValor() {
        return valor;
    }
}
