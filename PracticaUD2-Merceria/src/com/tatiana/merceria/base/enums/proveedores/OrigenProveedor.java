package com.tatiana.merceria.base.enums.proveedores;

public enum OrigenProveedor {
    china("CHINA"),
    india("INDIA"),
    estadosunidos("ESTADOS UNIDOS"),
    reinounido("REINO UNIDO"),
    españa("ESPAÑA");


    private String valor;

    OrigenProveedor(String valor){
        this.valor= valor;
    }

    public String getValor() {
        return valor;
    }
}
