package com.tatiana.merceria.base.enums.proveedores;

public enum NombreProveedor {
    RopaInteriorSl("Ropa Interior Sl"),
    TodoRopaInteriorMyCSociedadLimitada("Todo Ropa Interior M&C Sociedad Limitada"),
    RopaInteriorAnaSl("Ropa Interior Ana Sl"),
    EnjoyRopaSL("Enjoy Ropa S.L."),
    KorolukPaolaIrene("Koroluk Paola Irene"),
    FicottonSl("Ficotton Sl"),
    VittorioDiseñoSl("Vittorio Diseño Sl"),
    BoblanDiseñoSociedadLimitada("Boblan Diseño Sociedad Limitada");

    private String valor;

    NombreProveedor(String valor){
        this.valor= valor;
    }

    public String getValor() {
        return valor;
    }
}
