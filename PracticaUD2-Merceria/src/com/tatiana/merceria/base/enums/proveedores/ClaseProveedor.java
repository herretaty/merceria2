package com.tatiana.merceria.base.enums.proveedores;

public enum ClaseProveedor {
    boho("BOHO"),
    hipster("HIPSTER"),
    trendy("TRENDY"),
    casual("CASUAL"),
    Aatsy("ARTSY"),
    clásico("CLASICO"),
    exótico("EXOTICO"),
    glamoroso("GLAMOROSO"),
    ninguna("NINGUNA");


    private String valor;

    ClaseProveedor(String valor){
        this.valor= valor;
    }

    public String getValor() {
        return valor;
    }
}
