package com.tatiana.merceria.gui;

import com.tatiana.merceria.util.Util;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;

/**
 * @author TatianaHerrera
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase controlador
     * @param modelo
     * @param vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo= modelo;
        this.vista= vista;

        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addWindowListeners(this);
        refrescarTodo();
        iniciarTablaColor();

    }


    /**
     * Metodo para refrescar la informacion
     */
    private void refrescarTodo() {
        listarProductos();
        listarProveedores();
        listarVenta();
        refrescar=false;
    }

    /**
     * Agrega el ActionListener a los botones
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnAnnadirProducto.addActionListener(listener);
        vista.btnModificarProducto.addActionListener(listener);
        vista.btnEliminarProducto.addActionListener(listener);

        vista.btnAnnadirVenta.addActionListener(listener);
        vista.btnModificarVenta.addActionListener(listener);
        vista.btnEliminarVenta.addActionListener(listener);

        vista.btnBuscarVendedor.addActionListener(listener);
        vista.btnReferenciaProducto.addActionListener(listener);
        vista.btnOrdenarFecha.addActionListener(listener);
        vista.btnMostrar.addActionListener(listener);

        vista.btnAnnadirProveedor.addActionListener(listener);
        vista.btnModificarProveedor.addActionListener(listener);
        vista.btnEliminarProveedor.addActionListener(listener);


        vista.optionDialog.btnGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);

    }


    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel)e.getSource()).isSelectionEmpty()){
            if (e.getSource().equals(vista.jtProductos.getSelectionModel())){
                int fila=vista.jtProductos.getSelectedRow();
                vista.txtNombreProducto.setText(String.valueOf(vista.jtProductos.getValueAt(fila,1)));
                vista.txtColorProducto.setText(String.valueOf(vista.jtProductos.getValueAt(fila,2)));
                vista.comboMarcaProducto.setSelectedItem(String.valueOf(vista.jtProductos.getValueAt(fila,3)));
                vista.comboFabricanteProducto.setSelectedItem(String.valueOf(vista.jtProductos.getValueAt(fila,4)));
                vista.comboTallaProducto.setSelectedItem(String.valueOf(vista.jtProductos.getValueAt(fila,5)));
            }else if (e.getSource().equals(vista.jtProveedores.getSelectionModel())){
                int fila=vista.jtProveedores.getSelectedRow();
                vista.txtTelefono.setText(String.valueOf(vista.jtProveedores.getValueAt(fila,1)));
                vista.comboNombreProveedor.setSelectedItem(String.valueOf(vista.jtProveedores.getValueAt(fila,2)));
                vista.comboClaseProducto.setSelectedItem(String.valueOf(vista.jtProveedores.getValueAt(fila,3)));
                vista.comboOrigenProducto.setSelectedItem(String.valueOf(vista.jtProveedores.getValueAt(fila,4)));
            }else if (e.getSource().equals(vista.jtVenta.getSelectionModel())){
                int fila=vista.jtVenta.getSelectedRow();
                vista.txtNombreVendedor.setText(String.valueOf(vista.jtVenta.getValueAt(fila,1)));
                vista.txtCantidad.setText(String.valueOf(vista.jtVenta.getValueAt(fila,2)));
                vista.txtReferencia.setText(String.valueOf(vista.jtVenta.getValueAt(fila,3)));
                vista.comboProductos.setSelectedItem(String.valueOf(vista.jtVenta.getValueAt(fila,4)));
                vista.comboProveedor.setSelectedItem(String.valueOf(vista.jtVenta.getValueAt(fila,5)));
                vista.txtPrecio.setText(String.valueOf(vista.jtVenta.getValueAt(fila,6)));
                vista.fecha_factura.setDate((Date.valueOf(String.valueOf(vista.jtVenta.getValueAt(fila,7))).toLocalDate()));
            } else if (e.getValueIsAdjusting()&&((ListSelectionModel)e.getSource()).isSelectionEmpty()&&!refrescar){
                if (e.getSource().equals(vista.jtProductos.getSelectionModel())){
                    borrarCamposProductos();
                }else if (e.getSource().equals(vista.jtProveedores.getSelectionModel())){
                    borrarCamposProveedor();
                }else if (e.getSource().equals(vista.jtVenta.getSelectionModel())){
                    borrarCamposVenta();
                }
            }
        }

    }

    /**
     *Metodo donde se van a realizar todas las acciones
     * @param e
     */

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando=e.getActionCommand();
        switch (comando){
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                try {
                    modelo.desconectar();
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Abrir Opciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())){
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                }else{
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "Guardar":
                modelo.setPropValues(vista.optionDialog.txtIp.getText(),
                        vista.optionDialog.txtUsuario.getText(),
                        String.valueOf(vista.optionDialog.jpfContraseña.getPassword()),
                        String.valueOf(vista.optionDialog.jpfContraseñaAdministrador.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "Añadir Producto":
                try{
                    modelo.insertarProducto(
                            vista.txtNombreProducto.getText(),
                            vista.txtColorProducto.getText(),
                            String.valueOf(vista.comboMarcaProducto.getSelectedItem()),
                            String.valueOf(vista.comboFabricanteProducto.getSelectedItem()),
                            String.valueOf(vista.comboTallaProducto.getSelectedItem()));
                    listarProductos();
                    JOptionPane.showMessageDialog(null, "Producto Creado", "Info", JOptionPane.INFORMATION_MESSAGE);
                }catch (SQLException e1){
                    e1.printStackTrace();
                    vista.jtProductos.clearSelection();
                }
                break;
            case "Modificar Producto":
                try {
                    if (comprobarProductoVacio()){
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Info", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtProductos.clearSelection();
                    }else {
                        modelo.modificarProducto(
                                vista.txtNombreProducto.getText(),
                                vista.txtColorProducto.getText(),
                                String.valueOf(vista.comboMarcaProducto.getSelectedItem()),
                                String.valueOf(vista.comboFabricanteProducto.getSelectedItem()),
                                String.valueOf(vista.comboTallaProducto.getSelectedItem()),
                                Integer.parseInt((String.valueOf(vista.jtProductos.getValueAt(vista.jtProductos.getSelectedRow(),0)))));
                        listarProductos();
                        JOptionPane.showMessageDialog(null, "Producto Modificado", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Eliminar Producto":
                modelo.borrarProducto(Integer.parseInt((String) (vista.jtProductos.getValueAt(vista.jtProductos.getSelectedRow(), 0))));
                JOptionPane.showMessageDialog(null, "Producto Eliminado ", "Info", JOptionPane.INFORMATION_MESSAGE);
                listarProductos();
                break;
            case "Añadir Proveedor":
                try {
                    if (comprobarProveedorVacio()){
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Error", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtProveedores.clearSelection();
                    }else {
                        modelo.insertarProveedor(
                                vista.txtTelefono.getText(),
                                String.valueOf(vista.comboOrigenProducto.getSelectedItem()),
                                String.valueOf(vista.comboClaseProducto.getSelectedItem()),
                                String.valueOf(vista.comboNombreProveedor.getSelectedItem()));
                        listarProveedores();
                        JOptionPane.showMessageDialog(null, "Proveedor Añadido", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Modificar Proveedor":
                try {
                    if (comprobarProveedorVacio()){
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Info", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtProveedores.clearSelection();
                    }else {
                        modelo.modificarProveedor(
                                vista.txtTelefono.getText(),
                                String.valueOf(vista.comboOrigenProducto.getSelectedItem()),
                                String.valueOf(vista.comboClaseProducto.getSelectedItem()),
                                String.valueOf(vista.comboNombreProveedor.getSelectedItem()),
                                Integer.parseInt((String.valueOf(vista.jtProveedores.getValueAt(vista.jtProveedores.getSelectedRow(),0)))));
                        JOptionPane.showMessageDialog(null, "Proveedor Modificado", "Info", JOptionPane.INFORMATION_MESSAGE);
                        listarProveedores();
                    }
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Eliminar Proveedor":
                modelo.borrarProveedor(Integer.parseInt((String)vista.jtProveedores.getValueAt(vista.jtProveedores.getSelectedRow(),0)));
                JOptionPane.showMessageDialog(null,"Proveedor Eliminado", "Info", JOptionPane.INFORMATION_MESSAGE);
                listarProveedores();
                break;
            case "Añadir Venta":
                try {
                    if (comprobarVentaVacia()){
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Error", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtVenta.clearSelection();
                    }else {
                        modelo.insertarVenta(
                                vista.txtNombreVendedor.getText(),
                                vista.txtReferencia.getText(),
                                vista.txtCantidad.getText(),
                                String.valueOf(vista.comboProductos.getSelectedItem()),
                                String.valueOf(vista.comboProveedor.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fecha_factura.getDate());
                        listarVenta();
                        JOptionPane.showMessageDialog(null, "Venta Creada", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
            case "Modificar Venta":
                try {
                    if (comprobarVentaVacia()){
                        JOptionPane.showMessageDialog(null, "Rellena los campos", "Info", JOptionPane.INFORMATION_MESSAGE);
                        vista.jtVenta.clearSelection();
                    }else {
                        modelo.modificarVenta(
                                vista.txtNombreVendedor.getText(),
                                vista.txtReferencia.getText(),
                                vista.txtCantidad.getText(),
                                String.valueOf(vista.comboProductos.getSelectedItem()),
                                String.valueOf(vista.comboProveedor.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fecha_factura.getDate(),
                                Integer.parseInt((String.valueOf(vista.jtVenta.getValueAt(vista.jtVenta.getSelectedRow(),0)))));
                          listarVenta();
                          JOptionPane.showMessageDialog(null, "Venta Modificada", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Eliminar Venta":
                modelo.borrarVenta(Integer.parseInt((String.valueOf(vista.jtVenta.getValueAt(vista.jtVenta.getSelectedRow(),0)))));
                listarVenta();
                JOptionPane.showMessageDialog(null, "Venta eliminada", "Info", JOptionPane.INFORMATION_MESSAGE);
                break;
            case "Buscar Vendedor":
                String buscarVendedor= (vista.textBuscarVendedor.getText());
                try {
                    ResultSet resultSet= modelo.buscarVendedor(buscarVendedor);
                    JOptionPane.showMessageDialog(null, "Busqueda encontrada", "Info", JOptionPane.INFORMATION_MESSAGE);
                    cargarColumnasVenta(resultSet);
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Referencia Producto":
                try {
                    modelo.referenciaProducto();
                    cargarFilaReferencia(modelo.obtenerDatos());

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case"CrearTablaMerceria":
                try {
                    modelo.crearTablaMerceria1();
                    JOptionPane.showMessageDialog(null, "Tabla Creada", "Info", JOptionPane.INFORMATION_MESSAGE);
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;
            case "Ordenar Fecha":
                try {
                    modelo.ordenarFecha();
                    cargarFilasOrdenadas(modelo.obtenerDatos1());
                    JOptionPane.showMessageDialog(null, "Fechas Ordenadas", "Info", JOptionPane.INFORMATION_MESSAGE);

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case"Mostrar Todo":
                try {
                    vista.jtVenta.setModel(cargarColumnasVenta(modelo.consultarVenta()));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
        }
    }

    /**
     * Metodo para ordenar en la colmna de venta
     * @param obtenerDatos1
     * @throws SQLException
     */


    private void cargarFilasOrdenadas(ResultSet obtenerDatos1) throws SQLException{
        Object[] fila= new Object[1];
        vista.dtmVenta.setRowCount(0);
        while (obtenerDatos1.next()){
            fila[0]=obtenerDatos1.getObject(1);
            vista.dtmVenta.addRow(fila);

        }

    }

    /**
     * Inserta la informacion en la fila color
     * @param resultSet
     * @throws SQLException
     */

    private void cargarFilaReferencia(ResultSet resultSet) throws SQLException {
        Object[] fila= new Object[2];
        vista.dtmReferencia.setRowCount(0);
        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            vista.dtmReferencia.addRow(fila);
        }
    }
    /**
     * Agrega los titulos de las columnas a la tabla color
     */

    private void iniciarTablaColor(){
        String[] headers={"Referencia","Producto"};
        vista.dtmReferencia.setColumnIdentifiers(headers);
    }


    private void listarVenta() {
        try {
            vista.jtVenta.setModel(cargarColumnasVenta(modelo.consultarVenta()));

        }catch (SQLException e1){
            e1.printStackTrace();
        }
    }

    /**
     * Da la opcion de cargar las columnas que tengas
     * @param resultSet
     * @return
     * @throws SQLException
     */

    private DefaultTableModel cargarColumnasVenta(ResultSet resultSet)throws SQLException{
        ResultSetMetaData metaData = resultSet.getMetaData();

        Vector<String>nombreColumna=new Vector<>();

        int contadorColumna=metaData.getColumnCount();

        for (int columna=1;columna<=contadorColumna;columna++ ) {
            nombreColumna.add(metaData.getColumnName(columna));
        }

        Vector<Vector<Object>> data=new Vector<>();
        setDataVector(resultSet,contadorColumna,data);

        vista.dtmVenta.setDataVector(data,nombreColumna);
        return vista.dtmVenta;

    }


    private void listarProveedores() {
        try {
            vista.jtProveedores.setModel(cargarColumnasProveedor(modelo.consultarProveedor()));
            vista.comboProveedor.removeAllItems();
            for (int i=0; i<vista.dtmProveedor.getRowCount();i++){
                vista.comboProveedor.addItem(
                        vista.dtmProveedor.getValueAt(i,0)+" - "+
                                vista.dtmProveedor.getValueAt(i,1)+" , "+
                                vista.dtmProveedor.getValueAt(i,2)+" , "+
                                vista.dtmProveedor.getValueAt(i,3)+" , "+
                                vista.dtmProveedor.getValueAt(i,4));

            }
        }catch (SQLException e1){
            e1.printStackTrace();
        }
    }

    private DefaultTableModel cargarColumnasProveedor(ResultSet resultSet) throws SQLException{
        ResultSetMetaData metaData = resultSet.getMetaData();

        Vector<String>nombreColumna=new Vector<>();

        int contadorColumna=metaData.getColumnCount();

        for (int columna=1;columna<=contadorColumna;columna++ ) {
            nombreColumna.add(metaData.getColumnName(columna));
        }

        Vector<Vector<Object>> data=new Vector<>();
        setDataVector(resultSet,contadorColumna,data);

        vista.dtmProveedor.setDataVector(data,nombreColumna);
        return vista.dtmProveedor;
    }

    private void listarProductos() {
        try{
            vista.jtProductos.setModel(cargarColumnasProducto(modelo.consultarProducto()));
            vista.comboProductos.removeAllItems();
            for (int i=0; i<vista.dtmProductos.getRowCount();i++){
                vista.comboProductos.addItem(
                        vista.dtmProductos.getValueAt(i,0)+" - "+
                                vista.dtmProductos.getValueAt(i,5)+" , "+
                                vista.dtmProductos.getValueAt(i,4)+" , "+
                                vista.dtmProductos.getValueAt(i,3)+" , "+
                                vista.dtmProductos.getValueAt(i,2)+" , "+
                                vista.dtmProductos.getValueAt(i,1));
            }
        }catch (SQLException e1){
            e1.printStackTrace();
        }
    }


    private DefaultTableModel cargarColumnasProducto(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumna=new Vector<>();

        int contadorColumna=metaData.getColumnCount();

        for (int columna=1;columna<=contadorColumna;columna++ ) {
            nombreColumna.add(metaData.getColumnName(columna));
        }

        Vector<Vector<Object>> data=new Vector<>();
        setDataVector(rs,contadorColumna,data);

        vista.dtmProductos.setDataVector(data,nombreColumna);
        return vista.dtmProductos;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data)throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Comprueba los campos vacios
     * @return
     */

    private boolean comprobarVentaVacia() {
        return vista.txtNombreVendedor.getText().isEmpty()||
                vista.txtReferencia.getText().isEmpty()||
                vista.txtCantidad.getText().isEmpty()||
                vista.comboProductos.getSelectedIndex()==-1||
                vista.comboProveedor.getSelectedIndex()== -1||
                vista.txtPrecio.getText().isEmpty() ||
                vista.fecha_factura.getText().isEmpty();
    }
    /**
     * Comprueba los campos vacios
     * @return
     */

    private boolean comprobarProveedorVacio() {
        return
                vista.comboClaseProducto.getSelectedIndex()== -1||
                        vista.txtTelefono.getText().isEmpty()||
                vista.comboNombreProveedor.getSelectedIndex()== -1||
                vista.comboClaseProducto.getSelectedIndex()== -1;

    }
    /**
     * Comprueba los campos vacios
     * @return
     */

    private boolean comprobarProductoVacio() {
        return vista.txtNombreProducto.getText().isEmpty()||
                vista.txtColorProducto.getText().isEmpty()||
                vista.comboMarcaProducto.getSelectedIndex()== -1||
                vista.comboFabricanteProducto.getSelectedIndex()== -1||
                vista.comboTallaProducto.getSelectedIndex()== -1;
    }


    /**
     * Borra los campos
     */
    private void borrarCamposVenta() {
        vista.txtNombreVendedor.setText("");
        vista.txtReferencia.setText("");
        vista.txtCantidad.setText("");
        vista.comboProductos.setSelectedIndex(-1);
        vista.comboProveedor.setSelectedIndex(-1);
        vista.txtPrecio.setText("");
        vista.fecha_factura.setText("");

    }
    /**
     * Borra los campos
     */

    private void borrarCamposProveedor() {
        vista.txtTelefono.setText("");
        vista.comboClaseProducto.setSelectedIndex(-1);
        vista.comboNombreProveedor.setSelectedIndex(-1);
        vista.comboOrigenProducto.setSelectedIndex(-1);
    }
    /**
     * Borra los campos
     */

    private void borrarCamposProductos() {
        vista.txtNombreProducto.setText("");
        vista.txtTelefono.setText("");
        vista.comboMarcaProducto.setSelectedIndex(-1);
        vista.comboFabricanteProducto.setSelectedIndex(-1);
        vista.comboTallaProducto.setSelectedIndex(-1);
    }

    /**
     * Opciones donde metemos la informacion de usuario etc
     */

    private void setOptions() {
        vista.optionDialog.txtIp.setText(modelo.getIp());
        vista.optionDialog.txtUsuario.setText(modelo.getUser());
        vista.optionDialog.jpfContraseña.setText(modelo.getPassword());
        vista.optionDialog.jpfContraseñaAdministrador.setText(modelo.getAdminPassword());
    }

    /**
     * Metodos que no se usan
     * @param e
     */
    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }



}
