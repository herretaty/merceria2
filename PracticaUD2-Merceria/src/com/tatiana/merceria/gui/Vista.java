package com.tatiana.merceria.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.tatiana.merceria.base.enums.productos.MarcaProductos;
import com.tatiana.merceria.base.enums.productos.FabricanteProductos;
import com.tatiana.merceria.base.enums.productos.TallaProductos;
import com.tatiana.merceria.base.enums.proveedores.NombreProveedor;
import com.tatiana.merceria.base.enums.proveedores.OrigenProveedor;
import com.tatiana.merceria.base.enums.proveedores.ClaseProveedor;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 *  Clase Vista. Ventana principal del programa donde se realiza la mayoria de
 *  las acciones o tareas
 */

public class Vista extends JFrame {
    private  final static String TITULO="Merceria";

    private JTabbedPane tabbedPane;
    private JPanel panel1;


    //PRODUCTOS//
    JTable jtProductos;
    JComboBox<String>comboMarcaProducto;
    JComboBox<String> comboFabricanteProducto;
    JComboBox<String>comboTallaProducto;
    JButton btnAnnadirProducto;
    JButton btnEliminarProducto;
    JButton btnModificarProducto;

    //PROVEEDOR//
    JTable jtProveedores;
    JTextField txtTelefono;
    JComboBox<String> comboClaseProducto;
    JComboBox<String>comboOrigenProducto;
    JComboBox<String> comboNombreProveedor;
    JButton btnAnnadirProveedor;
    JButton btnEliminarProveedor;
    JButton btnModificarProveedor;

    //VENTA//
    JTable jtVenta;
    JTextField txtNombreVendedor;
    JTextField txtColorProducto;
    JComboBox comboProductos;
    JComboBox comboProveedor;
    DatePicker fecha_factura;
    JTextField txtPrecio;
    JButton btnAnnadirVenta;
    JButton btnEliminarVenta;
    JButton btnModificarVenta;
    JButton btnBuscarVendedor;
    JTextField textBuscarVendedor;
    JButton btnReferenciaProducto;
    JTable jtPrecioProducto;
    JButton btnOrdenarFecha;
    JTextField txtReferencia;
    JTextField txtCantidad;
    JTextField txtNombreProducto;
    JButton btnMostrar;


    //TABLAS//
    DefaultTableModel dtmProductos;
    DefaultTableModel dtmProveedor;
    DefaultTableModel dtmVenta;
    DefaultTableModel dtmReferencia;

    //OPTION DIALOG//
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    //MENU//
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    JMenuItem itemCrearTabla;

    /**
     * Constructor
     */
    public Vista(){
        super(TITULO);
        initFrame();
    }

    /**
     * Panel de la ventana
     */

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);

        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();


    }

    private void setTableModels() {
        this.dtmProductos=new DefaultTableModel();
        this.jtProductos.setModel(dtmProductos);

        this.dtmProveedor=new DefaultTableModel();
        this.jtProveedores.setModel(dtmProveedor);

        this.dtmVenta= new DefaultTableModel();
        this.jtVenta.setModel(dtmVenta);

        this.dtmReferencia= new DefaultTableModel();
        this.jtPrecioProducto.setModel(dtmReferencia);




    }

    private void setEnumComboBox() {
        //PRODUCTOS//

        for (TallaProductos constant:TallaProductos.values()){
            comboTallaProducto.addItem(constant.getValor());
        }
        comboTallaProducto.setSelectedIndex(-1);

        for (FabricanteProductos constant: FabricanteProductos.values()){
            comboFabricanteProducto.addItem(constant.getValor());
        }
        comboFabricanteProducto.setSelectedIndex(-1);

        for (MarcaProductos constant: MarcaProductos.values()){
            comboMarcaProducto.addItem(constant.getValor());
        }
        comboMarcaProducto.setSelectedIndex(-1);

        //PROVEEDORES//

        for (NombreProveedor constant: NombreProveedor.values()){
            comboNombreProveedor.addItem(constant.getValor());
        }
        comboNombreProveedor.setSelectedIndex(-1);

        for (OrigenProveedor constant: OrigenProveedor.values()){
            comboOrigenProducto.addItem(constant.getValor());
        }
        comboOrigenProducto.setSelectedIndex(-1);

        for(ClaseProveedor constant: ClaseProveedor.values()){
            comboClaseProducto.addItem(constant.getValor());
        }
        comboClaseProducto.setSelectedIndex(-1);



    }

    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("Abrir Opciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    /**
     * Crea un menu y le agrega los elementos necesarios para el programa
     */

    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemCrearTabla= new JMenuItem("Crear Tabla Merceria");
        itemCrearTabla.setActionCommand("CrearTablaMerceria");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        menu.add(itemCrearTabla);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }


}
