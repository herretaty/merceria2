package com.tatiana.merceria.gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {

    private String ip;
    private String root;
    private String password;
    private String adminPassword;

    /**
     * Constructor
     */
    public Modelo(){

        getPropValues();
    }

    /**
     * Getters y setters
     * @return
     */
    public String getIp() {
        return ip;
    }

    public String getUser() {
        return root;
    }

    public String getPassword() {
        return password;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    private Connection conexion;

    /**
     *Invoca el meto conectar
     */

    public void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/merceria", "root", "");
        } catch (SQLException sqlEx) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
                PreparedStatement statement = null;
                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();
                System.out.println("conexion realizada");
            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }

        }
    }

    private String leerFichero() throws IOException{
        try (BufferedReader reader = new BufferedReader(new FileReader("merceria_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }
    public void desconectar() throws SQLException{
        if (conexion!=null){
            conexion.close();
            conexion=null;
            System.out.println("Conexion cerrada");
        }
    }

    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            root = prop.getProperty("root");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void setPropValues(String ip, String root, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", root);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.root = root;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Añade nuevos datos a la bbdd
     * @param marcaProducto
     * @param fabricanteProducto
     * @param tallaProducto
     * @throws SQLException
     */
     void insertarProducto(String nombreProducto,String colorProducto, String marcaProducto, String fabricanteProducto, String tallaProducto) throws SQLException{
        String consultaSql= "INSERT INTO productos (productos.nombre_producto,productos.color_producto,marcaProducto,fabricante,tallaProducto) VALUES(?,?,?,?,?)";
        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consultaSql);
        sentencia.setString(1,nombreProducto);
        sentencia.setString(2,colorProducto);
        sentencia.setString(3,marcaProducto);
        sentencia.setString(4,fabricanteProducto);
        sentencia.setString(5,tallaProducto);

        sentencia.executeUpdate();
    }
    ResultSet consultarProducto() throws SQLException{
        String sentenciaSql= "SELECT concat(productos.id_producto) as 'Id', concat(nombre_producto) as 'Nombre Producto',concat(color_producto)as 'Color Producto',concat(marcaProducto) as 'Marca Producto', " +
                "concat(productos.fabricante) as 'Fabricante Productos', concat(tallaProducto)as 'Talla Producto'" +
                "FROM productos";
        PreparedStatement sentencia=null;
        ResultSet resultado=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        resultado=sentencia.executeQuery();
        return resultado;
    }
    void borrarProducto(int id_producto) {
        String sentenciaSql= "DELETE FROM productos WHERE id_producto=?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_producto);
            sentencia.executeUpdate();

        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     *  Realiza La modificacion de los datos seleccionados
     * @param marcaProducto
     * @param proveedorProducto
     * @param tallaProducto
     * @param id_producto
     * @throws SQLException
     */

     void modificarProducto(String nombreProducto,String colorProducto,String marcaProducto, String proveedorProducto, String tallaProducto, int id_producto) throws SQLException{
        String sentenciaSql= "UPDATE productos SET productos.nombre_producto=?,productos.marcaProducto=?,marcaProducto=?, fabricante=?, tallaProducto=? WHERE id_producto=?";
        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
         sentencia.setString(1,nombreProducto);
         sentencia.setString(2,colorProducto);
        sentencia.setString(3,marcaProducto);
        sentencia.setString(4,proveedorProducto);
        sentencia.setString(5,tallaProducto);
        sentencia.setInt(6,id_producto);
        sentencia.executeUpdate();
    }

    /**
     * Añade nuevos datos a la bbdd
     * @param nombre_proveedor
     * @param telefono
     * @param origen_proveedor
     * @param clase_proveedor
     * @throws SQLException
     */

    public void insertarProveedor(String nombre_proveedor, String telefono, String origen_proveedor, String clase_proveedor) throws SQLException{
        String sentenciaSql="INSERT INTO proveedores(nombre_proveedor, telefono, origen_proveedor,clase_proveedor )" +
                " VALUES (?,?,?,?)";
        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1,nombre_proveedor);
        sentencia.setString(2,telefono);
        sentencia.setString(3,origen_proveedor);
        sentencia.setString(4,clase_proveedor);
        sentencia.executeUpdate();
    }

     ResultSet consultarProveedor() throws SQLException{
        String sentenciaSql="SELECT concat(id_proveedor) as 'Id', concat(nombre_proveedor) as 'Nombre Proveedor', concat(telefono) as 'Telefono', concat(origen_proveedor) as 'Origen Proveedor'" +
                ",concat(clase_proveedor) as 'Clase Proveedor' FROM proveedores";
        PreparedStatement sentencia=null;
        ResultSet resultado=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Realiza La modificacion de los datos seleccionados
     * @param nombre_proveedor
     * @param telefono
     * @param origen_producto
     * @param clase_producto
     * @param id_proveedor
     * @throws SQLException
     */
     void modificarProveedor(String nombre_proveedor,String telefono, String origen_producto ,String clase_producto, int id_proveedor) throws SQLException{
       String sentenciaSql="UPDATE proveedores SET nombre_proveedor=?,telefono=?,origen_proveedor=?,clase_proveedor=? WHERE id_proveedor=?";
       PreparedStatement sentencia=null;

         sentencia=conexion.prepareStatement(sentenciaSql);
         sentencia.setString(1,nombre_proveedor);
         sentencia.setString(2,telefono);
         sentencia.setString(3,origen_producto);
         sentencia.setString(4,clase_producto);
         sentencia.setInt(5,id_proveedor);
         sentencia.executeUpdate();


    }

    /**
     * Elimina el dato seleccionado de la BBDD
     * @param id_proveedor
     */

     void borrarProveedor(int id_proveedor) {
        String sentenciaSql="DELETE FROM proveedores WHERE id_proveedor=?";
        PreparedStatement sentencia=null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,id_proveedor);
            sentencia.executeUpdate();
        }catch (SQLException e1){
            e1.printStackTrace();
        }finally {
            if (sentencia!=null){
                try {
                    sentencia.close();
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     * Elimina el dato seleccionado de la BBDD
     * @param id_venta
     */

     void borrarVenta(int id_venta) {
        String sentenciaSql="DELETE FROM venta WHERE id_venta=?";
        PreparedStatement sentencia=null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,id_venta);
            sentencia.executeUpdate();

        }catch (SQLException e1){
            e1.printStackTrace();
        }finally {
            if(sentencia!=null){
                try {
                    sentencia.close();

                }catch (SQLException e1){
                    e1.printStackTrace();
                }
            }
        }
    }

    /**
     * Realiza La modificacion de los datos seleccionados
     * @param nombre_vendedor
     * @param referencia
     * @param cantidad
     * @param productos
     * @param proveedor
     * @param precio
     * @param fecha_factura
     * @param id_venta
     * @throws SQLException
     */
    void modificarVenta(String nombre_vendedor, String referencia, String cantidad, String productos, String proveedor, float precio, LocalDate fecha_factura,
                        int id_venta) throws SQLException{
        String sentenciaSql="UPDATE venta SET nombre_vendedor=?,referencia=?,cantidad=?,id_producto=?,id_proveedor=?, precio=?, fecha_factura=?" +
                "WHERE id_venta=?";


        int id_producto=Integer.valueOf(productos.split("")[0]);
        int id_proveedor=Integer.valueOf(proveedor.split("")[0]);

        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1,nombre_vendedor);
        sentencia.setString(2,referencia);
        sentencia.setString(3,cantidad);
        sentencia.setInt(4,id_producto);
        sentencia.setInt(5,id_proveedor);
        sentencia.setFloat(6, precio);
        sentencia.setDate(7, Date.valueOf(fecha_factura));
        sentencia.setInt(8,id_venta);
        sentencia.executeUpdate();


    }

    /**
     * Añade nuevos datos a la bbdd
     * @param nombre_vendedor
     * @param referencia
     * @param cantidad
     * @param productos
     * @param proveedor
     * @param precio
     * @param fecha_factura
     * @throws SQLException
     */

     void insertarVenta(String nombre_vendedor, String referencia,String cantidad, String productos, String proveedor, float precio, LocalDate fecha_factura)
            throws SQLException{
        String consultaSql="INSERT INTO venta(nombre_vendedor, referencia, cantidad, id_producto, id_proveedor,precio, fecha_factura)"+"VALUES (?,?,?,?,?,?,?)";
        PreparedStatement sentencia=null;

        int id_producto=Integer.valueOf(productos.split("")[0]);
        int id_proveedor=Integer.valueOf(proveedor.split("")[0]);

        sentencia=conexion.prepareStatement(consultaSql);
        sentencia.setString(1,nombre_vendedor);
        sentencia.setString(2,referencia);
        sentencia.setString(3,cantidad);
        sentencia.setInt(4,id_producto);
        sentencia.setInt(5,id_proveedor);
        sentencia.setFloat(6,precio);
        sentencia.setDate(7, Date.valueOf(fecha_factura));
        sentencia.executeUpdate();
    }


     ResultSet consultarVenta() throws SQLException{
        String sentenciaSql="SELECT concat(id_venta) as 'Id', concat(nombre_vendedor) as 'Nombre Vendedor'," +
                "concat(referencia) as 'Referencia',concat(cantidad) as 'Cantidad',concat(id_producto) as 'Producto', concat(id_proveedor) as 'Proveedor'," +
                "concat(precio) as 'Precio',concat(fecha_factura) as 'Fecha Factura' FROM venta";
        PreparedStatement sentencia=null;
        ResultSet resultado=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Busca al vendedor
     * @param buscarVendedor
     * @return
     * @throws SQLException
     */


    public ResultSet buscarVendedor(String buscarVendedor) throws SQLException{
        if (conexion==null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consulta= "SELECT * FROM venta where nombre_vendedor=?";
                PreparedStatement sentencia=null;
                sentencia =conexion.prepareStatement(consulta);
        sentencia.setString(1,buscarVendedor);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Muestra cuantos colores hay
     * @throws SQLException
     */


    public void referenciaProducto() throws SQLException{
        String sentenciaSql="call mostrarReferencia()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Obtiene los datos de la referencia
     * @return
     * @throws SQLException
     */

    public ResultSet obtenerDatos() throws SQLException{
        if (conexion== null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consultaSql="SELECT referencia, COUNT(*)  FROM venta GROUP BY referencia";
        PreparedStatement sentencia= null;
        sentencia= conexion.prepareStatement(consultaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Crea un nueva tabla
     * @throws SQLException
     */

    public void crearTablaMerceria1() throws SQLException {
        conexion= null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/merceria","root","");
        String sentenciaSql="call crearTablaMerceria1()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Ordenada las fechas
     * @return
     * @throws SQLException
     */

    public ResultSet ordenarFecha()throws SQLException{
        String consultaSql="call ordenarFecha()";
        PreparedStatement sentencia= null;
        sentencia= conexion.prepareStatement(consultaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;


    }

    /**
     * Obtiene los datos de las fechas
     * @return
     * @throws SQLException
     */

    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion==null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consultaSql= "SELECT fecha_factura FROM venta order by fecha_factura";
        PreparedStatement sentencia= null;
        sentencia= conexion.prepareStatement(consultaSql);
        ResultSet resultado= sentencia.executeQuery();
        return resultado;
    }


}




