package com.tatiana.merceria.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class OptionDialog extends JDialog {
    private JPanel panel1;
    JPasswordField jpfContraseña;
    JPasswordField jpfContraseñaAdministrador;
    JTextField txtIp;
    JTextField txtUsuario;
    JButton btnGuardar;


    private Frame ownwer;

    public OptionDialog(Frame ownwer){
        super(ownwer,"Opciones",true);
        this.ownwer=ownwer;
        initDialog();
    }

    private void initDialog(){
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(ownwer);
    }

}
